Note: Certain details given in this document might or might not be out-of-date.
In particular, there hasn't really been any proper addition since the
introduction of WL920416 and the early PREROTT (ROTT0993) prototype.

The originally released WOLF3D.PRJ file was used as a base for the
various project files included in this source tree. Each of them can
be used for building a specific game version out of the ones above.

How to identify code changes (and what's this WOLFREV thing)?
-------------------------------------------------------------

Check out VERSION.H (and VERSION.EQU). Basically, each project
shall define an EXEDEF macro for the specific project. For instance,
WL6AP14.PRJ is configured to define `GAMEVER_EXEDEF_WL1AP14`.

In addition, for the C sources only (thus excluding the ASM sources),
there's a separate `GAMEVER_EXEDEF` macro defined.
For instance, in WL1AP14.PRJ, `GAMEVER_EXEDEF` is defined to WL1AP14.

Other than VERSION.H and VERSION.EQU, the `GAMEVER_EXEDEF` macros are
not used *anywhere*. Instead, other macros are used. Some of them
are old macros added to control specific features, like GOODTIMES,
UPLOAD and CARMACIZED. There are also a few new macros, with two
important ones being mentioned here.

`GAMEVER_NOAH3D` is defined for Super 3-D Noah's Ark, and only for
this game.

`GAMEVER_WOLFREV` is defined in all projects, with different values.
It is intended to represent a revision of development of
the Wolfenstein 3D codebase. Usually, this revision value is based on
some *guessed* date (e.g., an original modification date of the EXE),
but it does not have to be the case.

For instance, N3DWT10 uses a WOLFREV in-between the revisions
of WL6GT14A and WL6GT14B.

These are two good reasons for using WOLFREV as described above:

- WL1AP12 and WL6AP11 share the same code revision. However, WL1AP11
is of an earlier revision. Thus, the usage of WOLFREV can be
less confusing.
- WOLFREV is a good way to describe the early March 1992 build. While
it's commonly called just "alpha" or "beta", `GAMEVER_WOLFREV`
gives us a more explicit description.

Is looking for `#if (GAMEVER_WOLFREV <= GV_WR_...) (or >)` sufficient?
----------------------------------------------------------------------

Nope!

For instance, for a project with `GAMEVER_WOLFREV == GV_WR_SDMFG10`,
the condition `GAMEVER_WOLFREV <= GV_WR_SDMFG10` holds.
However, so does `GAMEVER_WOLFREV <= GV_WR_WJ6IM14`,
and also `GAMEVER_WOLFREV > GV_WR_WL1AP10`.
Furthermore, `PML_StartupXMS (ID_PM.C)` has two mentions of a bug fix
dating 10/8/92, for which the `GAMEVER_WOLFREV` test was chosen
appropriately. The exact range of `WOLFREV` values from this test
is not based on any specific build/release of an EXE.

What is this based on
---------------------

This codebase is based on the Wolfenstein 3D + Spear of Destiny sources
as originally released by John Carmack on 1995, while working as
a technical director for id Software. While the 1995 release
includes GAMEPAL.OBJ and SIGNON.OBJ data files, other versions
of these were extracted from original EXEs.

Alternative `GFXV_APO.H` definitions were used, taken off the
Wolf4SDL source port by Moritz "Ripper" Kroll. `GFXV_APO.H`
and AUDIOWL6.H were both modified as required.

Similarly, there is the `FOREIGN\JAPAN\GFXV_WJ6.H` header file, based on
recreated definitions coming from choksta.

Recreated code for Super 3-D Noah's Ark (DOS port) was added, based on earlier
research work done for the ECWolf source port by Braden Obrzut. Furthermore, it
turned out the original EXE contains a lot of debugging information, including
original function and variable names. The latter were included in the
recreated codebase. There are very few (local) variables for which original
names aren't shown in the debugging information. There were also attempts to
match some other symbols (mostly mentions of types), but given that the
stripped EXE image can be recreated as-is, and that internal timestamps would
differ from the originals anyway, these efforts were halted.

It should be noted that Super 3-D Noah's Ark was originally created as a SNES
title, based on the SNES port of Wolfenstein 3D. It was followed by the DOS
port, based on the original Wolfenstein 3D codebase for DOS, while a few
features from the original SNES title were implemented, like an auto-map
and an arsenal of up to 6 feeders (weapons), including hand feeding.

There are also a couple of more variable and functions names "borrowed" off
other sources and used for the recreation of Wolfenstein 3D v1.0 and the
March 1992 build, although most chances are these were originally found in
earlier revisions of the Wolfenstein 3D codebase under the same names:

- `screenpage, VW_SetDefaultColor, VW_FixRefreshBuffer`
and `VW_QuitDoubleBuffer`, from the Catacomb 3-D sources
(`VW_InitDoubleBuffer` is already mentioned in `ID_VH.H` as released on 1995).
- `screensplit`, from the Blake Stone: Planet Strike sources.

Some code pieces were also "borrowed" from Keen Dreams / Catacomb 3-D
in a similar manner:

- `CAL_ReadGrChunk` (not very far from a clone of `CAL_CacheGrChunk`).
- An earlier revision of `MM_ShowMemory`.
- Usage of hardcoded DMA channel in `ID_SD.C`.
- The check for TED launching in `US_Startup`.

How were the project files (and a bit more) modified from the original
----------------------------------------------------------------------

The released sources, including the project file, appear to be in a state close
to what was used to make the late Activision EXEs, so it's not a surprise that
the various versions were generally made in a kind-of reverse order
(although it's not exactly a linear order).

- The two projects for the Activision builds target the 386 architecture,
while the ones for all earlier builds target the 286.
- The locations of SIGNUP.OBJ and GAMEPAL.OBJ are updated. Different files may
be used for different game versions. In fact, while the palette is the same for
the various SOD builds, different OBJs are used, nevertheless.
This is because the palette is a whole segment in the FormGen SOD EXEs,
but just a part of the data segment (aka the dseg) in the Activision SOD EXE.
Similarly, the Wolf3D palette has its own segment in the Apogee releases
preceding v1.4, while it's a part of the dseg in the rest.
- Paths to "OBJ" directories are replaced, so each project generates objects in
its own subdirectory within "OBJ".
- Paths to development environment (INCLUDE and LIB directories) are modified.
As expected there are chances you will still want to edit these, depending on
your setup of development tools.
- VERSION.H is edited. Each project defines a version-identifier macro,
which is used to let the preprocessor define a few macros used in the
original codebase (like UPLOAD for the Apogee shareware release).
- This macro is separately added via a command line argument to the assembler
for `ID_VH_A.ASM` and `ID_VL_A.ASM`, though. For S3DNA, the same is also done
for `ID_SD_A.ASM`, `WHACK_A.ASM` and `WL_DR_A.ASM`. For all files, the reason
is that the global project setting doesn't seem to apply to ASM files.
These few ASM files do include a new VERSION.EQU file now. No such change
was done for any of the other ASM files because there's no need.
- A few chunks of `WL_ACT2.C` are relocated into the following three files,
which are `#included` from `WL_ACT2.C`: `WL_FPROJ.C`, `WL_FSMOK.C`,
`WL_SROCK.C`. This is done since the exact location of
each such chunk in `WL_ACT2.C` depends on the version
(basically pre-v1.4 Apogee releases vs all the rest),
and because this could otherwise lead to a lot of code duplication, with
possibly unexpected consequences for anybody tinkering with the code.
In fact, it happened while preparing the projects for Wolf3D v1.1+1.2!
- `WOLFHACK.C` and `WHACK_A.ASM` are removed from all non-Activision projects
(they seem to be used just in projects targeting the 386). Furthermore,
`WL_TEXT.C` is removed from the SOD FormGen projects. It is not removed from the
SOD Activision project, though (which seems to be based on the Wolf3D one).
- In general, the reconstructed pre-v1.4 Apogee projects files are based on the
(reconstructed) SOD demo project. A few more details are given later, but
`WL_TEXT.C` is back in these.
- The list of objects in the SOD demo and pre-v1.4 Apogee projects
(for linking purposes) has modified orderings. In all v1.4 releases
(Wolf3D and SOD), the following objects are linked in the given order:
`SIGNON`, `GAMEPAL`, `ID_CA`, `ID_IN`. In the SOD demo, though, the order of
linkage is: `ID_CA`, `ID_IN`, `SIGNON`, `GAMEPAL`. In addition, in the pre-v1.4
releases the order is given by: `ID_CA`, `GAMEPAL`, `SIGNON`, `ID_IN`.
- Almost all optimizations are removed in the pre-v1.4 projects (Wolf3D/SOD).
In the list of optimizations as shown in the BC++ 3.0 IDE,
only "Suppress redundant loads", "Jump optimization" and "Standard stack frame"
are toggled on. Furthermore, the remaining selected choices are:
"Register Variable" - "Automatic", "Common Subexpressions" - "No Optimization"
and "Optimize For" - "Speed".
- Some simple comparison of the SOD (non-demo) FormGen releases should be
done. Basically, in terms of compiled/assembled C/ASM sources, the early
and late v1.4 releases are identical. In addition, both were originally
built using Borland C++ 3.1, rather than 3.0 as used for v1.0. However,
not only the wrong version of "1.0" is shown in the signon screen for
the early v1.4 EXE, but the project files are significantly more similar.
In fact, there are good chances the exact same project file
was originally used with no actual change. This refers
to properties like the order of linkage of objects.
- For an earlier revision of this codebase, a custom tool named STRIPBSS
(or any comparable tool) was required in order to remove the BSS sections from
certain executables (basically a bunch of zeros appended to each EXE file's
end, mostly referring to variables initialized to zeros). Only while working
on Blake Stone later (in a separate codebase), a way to strip these right
from Borland C++ was found out. In the debugger settings, Source Debugging
had to be disabled, meaning debugging information wouldn't be appended to
the EXE. It's probably the case that the BSS section must be included if
debugging symbols are, and while tools like LZEXE91 may strip the debugging
information, they don't compress the BSS section. That is, they don't do
the job of the above mentioned STRIPBSS, being removing the chunk of zeros,
while appropriately increasing the amount of additional memory
the program needs as defined in the EXE's header.
- SODFG14A and SODFG14B are essentially the exact same revision.
The only real differences are the changes in SIGNON.OBJ
and the Borland C++ Source Debugging toggle.
- WL6GT14A was probably made after WJ6IM14. In terms of project files, though,
the only real differences are SIGNON.OBJ, as well as the definition of JAPAN
JAPAN in one project and GOODTIMES in the other one. There are also different
`GAMEVER_WOLFREV` values in use, although it wasn't necessary, technically.
- WL920312.PRJ is quite close to WL1AP10.PRJ. The early build lacks
`WL_INTER.C`. Furthermore, the signon *and* palette objects' data differ.
- There may be at least one other difference at the least. Obviously, source
code files other than VERSION.H are edited as required. This includes the
addition of the new header file `GFXV_APO.H` for the Apogee builds, with
resource definitions taken off the Wolf4SDL source port, as well as
a few more such headers for Super 3-D Noah's Ark, based on
definitions from ECWolf and debugging information.

A few final notes
-----------------

The source code as originally released by id Software seems to be in some
intermediate state in-between the late GT Interactive release of Wolf3D and
the Activision release. As in the former, the GT logo is shown in the signon
screen and some text is shown on quit (after changing to text mode). On the
other hand, the project file is closer to the one used here for the Activision
release of Wolf3D, having the source files `WOLFHACK.C` and `WHACK_A.ASM` added
to the project, as well as targeting 80386 rather than 80286. If a guess can be
made, maybe the changes were done while preparing the Japanese version of
Wolfenstein 3D for the PC-98, to be published by Imagineer. There were
at least a few more localized versions (apparently for DOS),
though, so this could apply to any of these.
