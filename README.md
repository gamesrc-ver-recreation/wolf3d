Modified Wolfenstein 3D sources with multiple reconstructed versions
====================================================================

This source tree covers a modification of the open-source release
of Wolfenstein 3D and Spear of Destiny, with the main goal being
a reconstruction of sources matching multiple released binaries.

In case you try making binaries from this codebase, the usual caveat is
that even with what appear to be the right tools, you might not get an exact
binary as it was released back in the days, for multiple technical reasons.

List of covered versions
------------------------

With a few exceptions, using the right tools, this codebase can be used for
accurately recreating executables from the following releases:

- Wolfenstein 3D Shareware v1.0+1.1+1.2+1.4, Apogee releases (with cheats).
- Wolfenstein 3D Registered v1.1+1.2+1.4, Apogee releases (with cheats).
- Wolfenstein 3D Retail v1.4, Imagineer (DOS/V) release
(Japanese localization).
- Wolfenstein 3D Retail v1.4, early GT Interactive release.
- Wolfenstein 3D Retail v1.4, id Software (Mindscape International) release
(basically the early GT Interactive EXE with a different signon screen logo).
- Wolfenstein 3D Retail v1.4, late GT Interactive release
(no three-letter code is shown after completing an episode).
- Wolfenstein 3D Retail v1.4, Activision release.
- Spear of Destiny demo v1.0, FormGen release.
- Spear of Destiny v1.0+early v1.4 (mistakenly labeled 1.0)+late v1.4,
FormGen releases (copy protected).
- Spear of Destiny v1.4, Activision release (no copy protection).

In addition, ignoring some differences in debugging symbols as stored in the
original EXE, this codebase includes recreated code for the following title:

- Super 3-D Noah's Ark (S3DNA) v1.0, Wisdom Tree release (DOS port).

Furthermore, two early Wolfenstein 3D prototype executables can be recreated:

- Wolfenstein 3D (Shareware), March 12 1992 prototype executable.
- Wolfenstein 3D (Registered), April 16 1992 prototype executable.

Finally, within a separate directory, a modification of the open-source
release of Wolfenstein 3D may be used for recreating the following
September 1993 executable. Newly created exe is expected to differ
in debugging symbols, albeit less significantly than in S3DNA's case:

- Rise of the Triad (actually still a modified Wolfenstein 3D),
September 13 1993 prototype executable; Also known as ROTT0993.

A person with the knowledge stated that 3 ROTT prototypes, including ROTT0993,
were supposed to be distributed with Rise of the Triad: Ludicrous Edition
at some point. As of writing this, it wasn't known if this would be done, but
it looked like a good hint that reconstructed ROTT0993 sources could be added
here. As of writing this, they're still in a separate directory for now,
without its unique code being in the usual codebase. Relatively speaking,
there wasn't a lot of unique code introduced to the open-source
release of Wolfenstein 3D, either way.

List of releases by project file names or output directory names
----------------------------------------------------------------

Note that this list excludes the early ROTT prototype exe, which
currently has a separate codebase residing under the subdirectory PREROTT.

- WL920312: Wolfenstein 3D (Shareware), March 12 1992 prototype executable.
- WL920416: Wolfenstein 3D (Shareware), April 16 1992 prototype executable.
- WL1AP10: Wolfenstein 3D Shareware v1.0, Apogee release.
- WL1AP11: Wolfenstein 3D Shareware v1.1, Apogee release.
- WL6AP11: Wolfenstein 3D Registered v1.1/1.2, Apogee release.
- WL1AP12: Wolfenstein 3D Shareware v1.2, Apogee release.
- SDMFG10: Spear of Destiny demo v1.0, FormGen release.
- SODFG10: Spear of Destiny v1.0, FormGen release.
- WL1AP14: Wolfenstein 3D Shareware v1.4, Apogee release (with cheats).
- WL6AP14: Wolfenstein 3D Registered v1.4, Apogee release (with cheats).
- SODFG14A: Spear of Destiny early v1.4 (labeled 1.0), FormGen release.
- SODFG14B: Spear of Destiny late v1.4, FormGen release.
- WJ6IM14: Wolfenstein 3D Retail v1.4, Imagineer release.
- WL6GT14A: Wolfenstein 3D Retail v1.4, early GT Interactive release.
- WL6ID14: Wolfenstein 3D Retail v1.4, id Software release.
- WL6GT14B: Wolfenstein 3D Retail v1.4, late GT Interactive release.
- N3DWT10: Super 3-D Noah's Ark v1.0, Wisdom Tree release.
- WL6AC14: Wolfenstein 3D Retail v1.4, Activision release.
- SODAC14: Spear of Destiny v1.4, Activision release.

Technical details of the original EXEs (rather than any recreated one)
----------------------------------------------------------------------

Unless SOD, ROTT or S3DNA is explicitly stated, the game in question is Wolf3D.

|        Version        |  Bytes |               MD5                |  CRC-32  |
|-----------------------|--------|----------------------------------|----------|
|   March 12 1992 exe   |  83375 | b5753a62c2495f2cdb2dfec909bd3e1c | 92b9a37f |
|   April 16 1992 exe   |  86540 | 4e5f27c4e3a5af154d64c2705a713c56 | 82c585d2 |
|     Shareware 1.0     |  94379 | a5a5fc03016b06ac6df5f3e302ca65b3 | 6c6bac36 |
|     Shareware 1.1     |  97605 | a10b4097904600600744a49d1c05ad8d | bef1a88b |
|  Registered 1.1/1.2   |  98402 | 9a1d0ba1e2224133966876993519806c | eeb3decc |
|     Shareware 1.2     |  97676 | 1b1146a8059130b428e4b5f8ba361fa0 | 3dc4b0ce |
|  SOD demo FormGen 1.0 | 108215 | 059c88f0e8b548de0595c4685d68d191 | 0d81eedc |
|    SOD FormGen 1.0    | 113273 | 6c85a773a3008842d465de9d664d7efa | beebc1f9 |
| Apogee shareware 1.4  | 109959 | 998945a6c0cedc2634b539a26b7f60a6 | 213d5d76 |
| Apogee registered 1.4 | 110715 | da82afe5e7c8098b1d3d762775d7853a | fbe11eb3 |
| SOD FormGen early 1.4 | 113396 | 40e5ddb7fff7f891dc2ba65a3299dd03 | 763f029d |
| SOD FormGen late 1.4  | 112835 | 0376eedb042a80387391a34b5ca4ce8e | f8dd4a07 |
|   Imagineer (DOS/V)   | 254046 | 29ffa709c5fe94776023e96691028e54 | 35a74dd2 |
| GT Interactive: Early | 259310 | e3cc1ffe63c05d53c2fac7f829e3f97c | cf660f1b |
|  Mindscape Int. / id  | 259310 | 8ed31fa77d893d8553279af131fdb296 | ded9ff6f |
| Pre-ROTT Sep 13 1993  | 500959 | 86b2f87e6a0df34e08ca98d74e638bc2 | b8905550 |
| GT Interactive: Late  | 109589 | 56f24e74ad4fe1836b0ac9047e59df78 | fcdf5431 |
|   S3DNA Wisdom Tree   | 448468 | c135c83acd3fd86310920c1807a0288a | bac8f80a |
|   Wolf3D Activision   | 108779 | ce402023d1c5dc16307c62df639940cb | 6d818dbe |
|    SOD Activision     | 113418 | 066edc6f078b283101c26e8b412c6576 | 4d38a060 |

Prerequisites for building each EXE
-----------------------------------

Required tools:

- Borland C++ 3.0 (and no other version), for all pre-v1.4 releases of
Wolfenstein 3D and Spear of Destiny (including 1992 prototype builds).
- Borland C++ 3.1 (exactly this one) for any other release.
- PKLITE 1.05 for the April 16 1992 prototype EXE.
- LZEXE 0.91e (English version, NOT 0.91) for the Activision EXEs.
- LZEXE 0.91 (French version, NOT 0.91e) for other
non-Activision EXEs that require EXE packing.

Notes before trying to build
----------------------------

- Even with usage of the right tools, getting executables matching
the originals in layout might depend on luck. Just for one example of a
known issue, there were instances in which `WL_GAME.OBJ` or `WL_PLAY.OBJ`
had to be deleted and rebuilt.
- There's no separate WL6AP12 project. You should look for WL6AP11, since
versions 1.1 and 1.2 of Registered Wolf3D share the same EXE.
- There's also no separate project for any cheats-disabled Apogee EXE.
Originally, the cheats were removed by opening each EXE in a hex editor and
replacing the "goobers" command-line code with seven spaces. If this is rather
done by modifying the sources, the generated EXE layout is expected to be
different, given that the "Duplicate strings merged" option is toggled on
(and possibly also if it weren't originally toggled).
- Due to apparent Borland C++ 3.0 bugs, when it's used to build a project for
the first time (no DSK or SYM file), there are chances the build process will
stop with unexpected compilation error/warning messages. If things are working
as expected, you should select the error in the Message window to see what is
its "cause" in the sources and jump to that location in the code
(you should be able to press on the "Enter" key in order to do this),
and then retry to compile from there. Repeat until an EXE is built.
- If there are other weird problems, try removing the SYM file and then
rebuilding. Remember, though, that luck is important here, and again you
may fail to get an EXE which is clearly close to an original one.

Building any version differing from the one under PREROTT
---------------------------------------------------------

1. Ensure the matching Borland C++ version is ready in the environment.
2. Use DOBUILD.BAT, selecting the output directory name and project name
(e.g., WL1AP10). Press on F9 to build after Borland C++ is loaded.
3. Note that you might or might not need to rebuild `WL_PLAY.OBJ`
for SOD FormGen v1.0. Similarly, you might (or might not) have to
rebuild `WL_GAME.OBJ` in the cases of the two SOD FormGen v1.4 revisions.
4. If an Activision EXE is built, pack it using LZEXE 0.91e (not 0.91).
If the April 16 1992 EXE is built, pack it with PKLITE 1.05.
Otherwise, use LZEXE 0.91 (not 0.91e) for all executables differing from
these ones: Imagineer, early GT, id Software / Mindscape International, S3DNA.

Hopefully, you should get exactly the original EXE, with a few exceptions:
- For SOD FormGen v1.0, the released EXE has the "LZ91" string replaced with
four NUL characters, and you might also find a difference in a couple of
more bytes inside the EXE header, specifying an amount of additional memory
the program needs (measured in paragraphs, i.e., 16-bytes groups). 
- The early SOD FormGen v1.4 EXE is expected to have an additional NUL byte
appended to its end, compared to a newly built matching EXE made with
Borland C++ 3.1 and packed with LZEXE 0.91.
- Debugging information is expected to differ for S3DNA.

Building the prototype ROTT executable under PREROTT
----------------------------------------------------

1. Ensure Borland C++ 3.1 is ready in the environment.
2. Enter the PREROTT directory and manually open WOLF3D.PRJ using BC.EXE.
Press on F9 to build after Borland C++ is loaded.
3. Note that you might or might not need to rebuild `WL_GAME.OBJ`.

Hopefully, you should get exactly the original EXE, short of differences
in debugging information. While it should be more accurate that S3DNA's,
a few differences are still expected, even after adjusting files' timestamps.
